package steps;

import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;

public class steps_put {


    @Quando("eu enviar uma requisição PUT para o {string} com {string}, {string}, {string}, {string}, {string}")
    public void euEnviarUmaRequisiçãoPUTParaOCom(String nome, String cpf, String email,
                                                 String valor, String parcelas, String seguro) {

        boolean b1 = Boolean.parseBoolean(seguro);
        int parc = Integer.parseInt(parcelas);
        float val = Float.parseFloat(valor);

        String myJson =
                "{\"nome\":\"" + nome + "\"," +
                        "\"cpf\": \"" + cpf + "\", " +
                        "\"email\": \"" + email + "\"," +
                        "\"valor\": \"" + val + "\"," +
                        "\"parcelas\": \"" + parc + "\"," +
                        "\"seguro\": \"" + b1 + "\"}";

        utils.setHttpRequest(RestAssured.given().contentType("application/json").body(myJson));

        utils.setResponse(utils.getHttpRequest().put(utils.getUrl()));
    }

    @E("o {string} com os dados alterado")
    public void oComOsDadosAlterado(String cpf) {
        Assertions.assertEquals(utils.getResponse().jsonPath().get("cpf"), cpf);
    }

    @E("a mensagem com o {string} não encontrado")
    public void aMensagemComONãoEncontrado(String cpf) {
        String mensagem = "CPF "+cpf+" não encontrado";
        Assertions.assertEquals(utils.getResponse().jsonPath().get("mensagem"), mensagem);
    }
}