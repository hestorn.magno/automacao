package steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;

public class steps_get {

    @Quando("realizar uma consulta com {string}")
    public void realizarUmaConsultaComCpf(String cpf) {
        RestAssured.baseURI = utils.getUrl();
        utils.setHttpRequest(RestAssured.given());
        utils.setResponse(utils.getHttpRequest().request(Method.GET, "/"+cpf+""));
    }    

    @E("a mensagem de restrição {string}")
    public void aMensagemDeRestricao(String cpf) {
    
        String textoValidacao = "O CPF "+cpf+" tem problema";
        Assertions.assertEquals(utils.getResponse().jsonPath().get("mensagem").toString(), textoValidacao);
    }  

  
    @Quando("realizar uma consulta")
    public void realizarUmaConsulta() {
        RestAssured.baseURI = utils.getUrl();
        utils.setHttpRequest(RestAssured.given());
        utils.setResponse(utils.getHttpRequest().request(Method.GET, ""));
    }

    @E("a lista de simulacoes cadastradas")
    public void aListaDeSimulacoesCadastradas() {
        utils.getResponse().then().assertThat().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schema_getSimulations.json"));
    }

    @Quando("realizar uma requisição GET passando o {string}")
    public void realizarUmaRequisicaoGETPassandoO(String cpf) {
        RestAssured.baseURI = utils.getUrl();
        utils.setHttpRequest(RestAssured.given());
        utils.setResponse(utils.getHttpRequest().request(Method.GET,"/"+cpf+""));
    }

    @E("os dados da simulação cadastrada")
    public void osDadosDaSimulacaoCadastrada() {
        utils.getResponse().then().assertThat().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schema_getSingleSimulation.json"));
    }

    @Entao("a API irá retornar o codigo 204")
    public void aAPIIraRetornarOCodigo204() {
        Assertions.assertEquals(utils.getResponse().getStatusCode(), 204);
    }

}
