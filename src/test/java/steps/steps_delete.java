package steps;

import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import org.junit.jupiter.api.Assertions;

public class steps_delete {

    @Quando("eu enviar uma requisição DELETE com a {string} da pessoa")
    public void euEnviarUmaRequisiçãoDELETEComADaPessoa(String id) {
        utils.setHttpRequest(RestAssured.given());
        utils.setResponse(utils.getHttpRequest().delete(utils.getUrl()+"/"+id));
    }

    @Quando("eu enviar uma requisição DELETE com a {string} de uma pessoa que não existe")
    public void euEnviarUmaRequisiçãoDELETEComADeUmaPessoaQueNãoExiste(String id) {
        utils.setHttpRequest(RestAssured.given());
        utils.setResponse(utils.getHttpRequest().delete(utils.getUrl()+"/"+id));
    }

    @E("a mensagem de nao encontrada")
    public void aMensagemDeNaoEncontrada() {
        String mensagem = "Simulação não encontrada";
        Assertions.assertEquals(utils.getResponse().jsonPath().get("mensagem"), mensagem);
    }
}
