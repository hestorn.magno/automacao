#language: pt

Funcionalidade:
  Validar diferentes operações GET na API.

  Esquema do Cenário: Consultar restrição pelo CPF - CPF Existe
    Dado o endpoint da API de restricoes
    Quando realizar uma consulta com "<CPF>"
    Entao a API ira retornar o codigo 200
    E a mensagem de restrição "<CPF>"
    Exemplos:
      |CPF|
      |97093236014|

  Esquema do Cenário: Consultar restrição pelo CPF - CPF não existente
    Dado o endpoint da API de restricoes
    Quando realizar uma consulta com "<CPF>"
    Entao a API irá retornar o codigo 204
    Exemplos:
      | CPF     |
      | 97093236013 |

  Cenario: Consultar todas as simulacoes cadastradas
    Dado  o endpoint da API simulacao
    Quando realizar uma consulta
    Entao a API ira retornar o codigo 200
    E a lista de simulacoes cadastradas

  Esquema do Cenário: Consultar uma simulacao pelo CPF
    Dado  o endpoint da API simulacao
    Quando realizar uma requisição GET passando o "<CPF>"
    Entao a API ira retornar o codigo 200
    E os dados da simulação cadastrada
    Exemplos:
    |CPF|
    |66414919004|

