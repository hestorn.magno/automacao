#language: pt

Funcionalidade:
  Validar inclusão de pessoa através do método POST.


  Esquema do Cenário: Criar uma simulacao
    Dado o endpoint da API simulacao
    Quando eu enviar uma requisição POST com "<nome>", "<cpf>", "<email>", "<valor>", "<parcela>", "<seguro>"
    Entao a API ira retornar o codigo 201
    E os dados da request contendo o "<cpf>" cadastrado
    Exemplos:

    |nome             |   |cpf          |   |email            |   |valor   | |parcela   |   |seguro  |
    |Lucas SeguroOn   |   |11122233350  |   |teste@email.com  |   |2000.50 | |3         |   |True    |
    |Lucas SeguroOff  |   |11122233351  |   |teste@email.com  |   |1500    | |5         |   |False    |

  Esquema do Cenário: Tentar criar simulacao com CPF repetido
    Dado o endpoint da API simulacao
    Quando eu enviar uma requisição POST com "<nome>", "<cpf>", "<email>", "<valor>", "<parcela>", "<seguro>"
    Entao a API ira retornar o codigo 400
    E a mensagem de CPF repetido
    Exemplos:

      |nome          |   |cpf          |   |email            |   |valor      | |parcela   |   |seguro  |
      |Lucas         |   |11122233346  |   |teste@email.com  |   |2229.99    | |3         |   |True    |